<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class crudController extends Controller
{
    
    public function create(){
        return view('crud');
    }

    public function store(Request $req){
       $this->validate($req, [
           'name' =>'required',
           'email' =>'required',           
           'telephone' =>'required|numeric',
           'dateOfBirth' =>'required',
           'gender' =>'required',
           'address' =>'required',
       ]);
       $name=$req->name;
       $email=$req->email;
       $telephone=$req->telephone;
       $dateOfBirth=$req->dateOfBirth;
       $gender=$req->gender;
       $address=$req->address;

       $data = "\n$name,$email,$telephone,$dateOfBirth,$gender,$address";
       $nameFile = "data/".preg_replace('/\s+/', '', $name).'-'.date("dmYHis").'.txt'; 
       $fh = fopen($nameFile, "a");
       fwrite($fh, $data);
       fclose($fh);
       return "Terima kasih telas mengisi form.</br><a href='/crud'>Kembali ke Index CRUD Biodata >></a>";
    }

    public function detail($nama){
        $txt_file    = file_get_contents('data/'.$nama.'.txt');
        $rows        = explode("\n", $txt_file);
        $data=array_shift($rows);        
        return view('crudDetail',['rows'=>$rows, 'namaFile'=>$nama]);
    }

    public function delete($nama){        
        unlink('data/'.$nama.'.txt');
        return "Data Berhasil Dihapus.</br><a href='/crud'>Kembali ke Index CRUD Biodata >></a>";
    }

    public function update(Request $req, $nama){
        $this->validate($req, [
            'name' =>'required',
            'email' =>'required',           
            'telephone' =>'required|numeric',
            'dateOfBirth' =>'required',
            'gender' =>'required',
            'address' =>'required',
        ]);
        $name=$req->name;
        $email=$req->email;
        $telephone=$req->telephone;
        $dateOfBirth=$req->dateOfBirth;
        $gender=$req->gender;
        $address=$req->address;

        $data = "\n$name,$email,$telephone,$dateOfBirth,$gender,$address";
        $nameFile = "data/".$nama.'.txt'; 
        $fh = fopen($nameFile, "w");
        fwrite($fh, $data);
        fclose($fh);

        return "Sudah Update";
    }




}
