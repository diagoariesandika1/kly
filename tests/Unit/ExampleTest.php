<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function createData(){
       $name="diago";
       $email="dia@gmail.com";
       $telephone="085235544433";
       $dateOfBirth="1996-03-12";
       $gender="pria";
       $address="Brimo kadungasem";

       $data = "\n$name,$email,$telephone,$dateOfBirth,$gender,$address";
       $nameFile = "data/".preg_replace('/\s+/', '', $name).'.txt'; 
       $fh = fopen($nameFile, "a");
       fwrite($fh, $data);
       fclose($fh);
    } 

    public function testGetAll()
    {
      $this->createData();

      $this->get('/course/diago');
      $this->seeStatusCode(200);
    }

}
