<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CRUDTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

     use DatabaseMigrations;

    public function createData(){
       $name="diago";
       $email="dia@gmail.com";
       $telephone="085235544433";
       $dateOfBirth="1996-03-12";
       $gender="pria";
       $address="Brimo kadungasem";

       $data = "\n$name,$email,$telephone,$dateOfBirth,$gender,$address";
       $nameFile = "data/".preg_replace('/\s+/', '', $name).'.txt'; 
       $fh = fopen($nameFile, "a");
       fwrite($fh, $data);
       fclose($fh);
    } 

    public function testGetAll()
    {
      $this->createData();

      $this->get('/course/diago');
      $this->seeStatusCode(200);
      $this->seeJsonStructure([       
      ]);
    }

    // public function testFindOne(){
    //   $this->createData();

    //   $this->get('/course/1');
    //   $this->seeStatusCode(200);
    //   $this->seeJsonStructure([
    //       'data' => [
    //         'id_trainer' ,      
    //         'id_category' ,
    //         'title' ,
    //         'description' ,
    //         'image_cover'
    //       ]
    //   ]);
    // }

    // public function testUpdate(){
    //   $this->createData();

    //   $this->put('/course/1',[
    //     'id_trainer' => 2,      
    //     'id_category' => 4,
    //     'title' => 'Meong',
    //     'description' => 'Bunyi Kucing',
    //     'image_cover' => 'http:inter',
    //     'updated_at' => Carbon::now()
    //   ]);

    //   $this->seeStatusCode(200);
    //   $this->seeJsonStructure([
    //       'message',
    //       'data' => [
    //         'id_trainer' ,      
    //         'id_category' ,
    //         'title' ,
    //         'description' ,
    //         'image_cover'
    //       ]
    //   ]);
    // }

    // public function testCreate(){
    //   $this->post('/course/', [
    //     'id_trainer' => 2,      
    //     'id_category' => 4,
    //     'title' => 'Meong',
    //     'description' => 'Bunyi Kucing',
    //     'image_cover' => 'http:inter',
    //   ]);

    //   $this->seeStatusCode(201);
    //   $this->seeJsonStructure([
    //     'data' => [
    //       'id_trainer' ,      
    //       'id_category' ,
    //       'title' ,
    //       'description' ,
    //       'image_cover'
    //     ]
    //   ]);
    // }

    // public function testDelete(){
    //   $this->createData();

    //   $this->delete('/course/3',[]);
    //   $this->seeStatusCode(200);
    //   $this->seeJsonStructure([
    //       'message',
    //       'data'
    //   ]);
    // }

}
