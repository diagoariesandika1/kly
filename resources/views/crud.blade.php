<form class="" action="{{url('crud')}}" method="post">
@if ($errors->any())
    <div style="color:red;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <table>
        <tr> <td> Nama </td> <td> :  <input required type="text" name="name" placeholder="Nama Lengkap"></td> </tr>
        <tr> <td> Email </td><td> :  <input required type="email" name="email"> </td></tr>
        <tr> <td>Tanggal Lahir </td> <td> :   <input required type="date" name="dateOfBirth"></td> </tr>
        <tr> <td>No Telepon </td> <td> :  <input required type="text" name="telephone"></td> </tr>
        <tr> <td>Gender </td> <td> :     <select class="form-control"  name="gender" style="width<td> : 200px;" required>
                        <option value="" style="color: #676768;" disable >Belum Terpilih</option>
                        <option value="pria" >Laki - laki</option>
                        <option value="wanita" >Perempuan</option>
                    </select> </td> </tr>
        <tr> <td>Alamat </td> <td> <span style="position:relative;height:25px;border:1px solid">:</span>  &nbsp; <textarea required name="address" rows="4" cols="50" required></textarea> </td> </tr>
        <tr><td><br></td></tr>
        <tr>
            <td>
                <input type="reset" value="Reset"> &emsp;
            </td>
            <td><input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <input type="submit" value="Submit">
            </td>
         </tr>
    </table>
</form