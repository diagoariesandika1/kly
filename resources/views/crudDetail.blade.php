<?php
echo '<h2>Isi Detail Data</h2>';
        foreach($rows as $row => $data)
        {
            $row_data = explode(',', $data);        
            $info[$row]['name']           = $row_data[0];
            $info[$row]['email']         = $row_data[1];
            $info[$row]['telephone']          = $row_data[2];
            $info[$row]['dateOfBirth']         = $row_data[3];
            $info[$row]['gender']       = $row_data[4];
            $info[$row]['address']       = $row_data[5];        
        }
?>

<form class="" action="{{url('crud').'/'.$namaFile}}" method="post">
    <table>
        <tr> <td> Nama </td> <td> :  <input required type="text" name="name" placeholder="Nama Lengkap" value="{{$info[$row]['name']}}"></td> </tr>
        <tr> <td> Email </td><td> :  <input required type="email" name="email" value="{{$info[$row]['email']}}"> </td></tr>
        <tr> <td>Tanggal Lahir </td> <td> :   <input required type="date" name="dateOfBirth" value="{{$info[$row]['dateOfBirth']}}"></td> </tr>
        <tr> <td>No Telepon </td> <td> :  <input required type="text" name="telephone" value ="{{$info[$row]['telephone']}}"></td> </tr>
        <tr> <td>Gender </td> <td> : 
                    <select class="form-control"  name="gender" style="width<td> : 200px;" required>                      
                        <?php if($info[$row]['gender']='pria'){ ?>
                            <option value="pria" selected>Laki - laki</option>
                            <option value="wanita" >Perempuan</option>
                        <?php } else { ?>
                            <option value="pria">Laki - laki</option>
                            <option value="wanita" selected>Perempuan</option>
                        <?php } ?>
                    </select> </td> </tr>
        <tr> <td>Alamat </td> <td> &nbsp; <textarea required name="address" rows="4" cols="50" required>{{$info[$row]['address']}}</textarea> </td> </tr>
        <tr><td><br></td></tr>
        <tr>
            <td><input name="_method" type="hidden" value="put"/>
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <input type="submit" value="Update Data" onclick="return confirm('Do you want to UPDATE? Y/N')">
            </td>
            <td><input name="_method" type="hidden" value="delete"/>
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <input type="submit" value="Delete This Data"  onclick="return confirm('Do you want to DELETE? Y/N')">
            </td>
         </tr>      
    </table>
</form>